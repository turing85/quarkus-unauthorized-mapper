package de.turing85;

import static io.restassured.RestAssured.given;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusIntegrationTest;
import javax.ws.rs.core.Response.Status;
import org.junit.jupiter.api.Test;

@QuarkusIntegrationTest
@TestHTTPEndpoint(HelloResource.class)
public class HelloResourceIT {

  @Test
  void anonymousCanNotRead() {
    // @formatter:off
    given()
        .when()
            .get()
        .then()
            .statusCode(Status.UNAUTHORIZED.getStatusCode());
    // @formatter:on
  }

  @Test
  void anonymousCanNotWrite() {
    // @formatter:off
    given()
        .when()
            .post()
        .then()
            .statusCode(Status.UNAUTHORIZED.getStatusCode());
    // @formatter:on
  }

  @Test
  void readerCanRead() {
    // @formatter:off
    given()
        .auth()
            .basic("reader", "reader")
        .when()
            .get()
        .then()
            .statusCode(Status.OK.getStatusCode());
    // @formatter:on
  }

  @Test
  void readerCanNotWrite() {
    // @formatter:off
    given()
        .auth()
            .basic("reader", "reader")
        .when()
            .post()
        .then()
            .statusCode(Status.FORBIDDEN.getStatusCode());
    // @formatter:on
  }

  @Test
  void writerCanNotRead() {
    // @formatter:off
    given()
        .auth()
            .basic("writer", "writer")
        .when()
            .get()
        .then()
            .statusCode(Status.FORBIDDEN.getStatusCode());
    // @formatter:on
  }

  @Test
  void writerCanWrite() {
    // @formatter:off
    given()
        .auth()
            .basic("writer", "writer")
        .when()
            .post()
        .then()
            .statusCode(Status.OK.getStatusCode());
    // @formatter:on
  }

  @Test
  void adminCanRead() {
    // @formatter:off
    given()
        .auth()
            .basic("admin", "admin")
        .when()
            .get()
        .then()
            .statusCode(Status.OK.getStatusCode());
    // @formatter:on
  }

  @Test
  void adminCanWrite() {
    // @formatter:off
    given()
        .auth()
            .basic("admin", "admin")
        .when()
            .post()
        .then()
            .statusCode(Status.OK.getStatusCode());
    // @formatter:on
  }
}