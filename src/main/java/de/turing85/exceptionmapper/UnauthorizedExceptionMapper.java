package de.turing85.exceptionmapper;

import io.quarkus.security.UnauthorizedException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class UnauthorizedExceptionMapper
    extends io.quarkus.resteasy.runtime.UnauthorizedExceptionMapper
    implements ExceptionMapper<UnauthorizedException> {

  @Override
  public Response toResponse(UnauthorizedException exception) {
    return Response.fromResponse(super.toResponse(exception))
        .entity("unauthorized")
        .build();
  }
}
