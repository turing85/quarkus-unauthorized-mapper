package de.turing85;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("hello")
@Produces(MediaType.TEXT_PLAIN)
public class HelloResource {

    private static final String HELLO = "Hello";

    @GET
    @RolesAllowed("READER")
    public String get() {
        return HELLO;
    }

    @POST
    @RolesAllowed("WRITER")
    public String post() {
        return HELLO;
    }
}