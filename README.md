# quarkus-unauthorized-mapper Project

This application demonstrates some weird behaviour when implementing an
[`ExceptionMapper<UnauthorizedException>`][exceptionMapperApi].

## Application

In our [pom.xml][pom-xml], we have configured `quarkus-elytron-security-properties-file`:
```xml
    ...
    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-elytron-security-properties-file</artifactId>
    </dependency>
    ...
```
And in our [`application.yml`][application-yml], we have set up some users:
```yaml
quarkus:
  security:
    users:
      embedded:
        enabled: true
        plain-text: true
        users:
          admin: admin
          reader: reader
          writer: writer
        roles:
          admin: READER,WRITER
          reader: READER
          writer: WRITER
```
Furthermore, we have written two `ExceptionMapper`s: one for `io.quarkus.security.ForbiddenException`
in [`ForbiddenExceptionMapper`][forbiddenExceptionMapper-java]:
```java
@Provider
public class ForbiddenExceptionMapper implements ExceptionMapper<ForbiddenException> {

  @Override
  public Response toResponse(ForbiddenException exception) {
    return Response
        .status(Status.FORBIDDEN.getStatusCode())
        .entity("forbidden")
        .build();
  }
}
```
and one for `io.quarkus.security.UnauthorizedException` in
[`UnauthorizedExceptionMapper`][unauthorizedExceptionMapper-java]:
```java
@Provider
// one more that io.quarkus.resteasy.runtime.UnauthorizedExceptionMapper's priority:
// @Priority(5002)
public class UnauthorizedExceptionMapper implements ExceptionMapper<UnauthorizedException> {

  @Override
  public Response toResponse(UnauthorizedException exception) {
    return Response
        .status(Status.UNAUTHORIZED.getStatusCode())
        .entity("unauthorized")
        .build();
  }
}
```
## Analysis
### 1st test: swagger-ui and curl

When we start the application:
```
./mvnw quarkus:dev
```
and access [`http://localhost:8080/q/swagger-ui`][swagger-localhost], we are able to interact with
the application just fine and as expected. We can even evaluate the behaviour through
`cURL`-commands, and the application behaves as expected:

```bash
# GET for anonymous, results in 401 UNAUTHORIZED
curl -X GET http://localhost:8080/hello -H "accept: text/plain" -v

# POST for anonymous, results in 401 UNAUTHORIZED
curl -X POST http://localhost:8080/hello -H "accept: text/plain" -d "" -v

# GET for reader, results in 200 OK
curl -X GET http://localhost:8080/hello -H "accept: text/plain" -H "Authorization: Basic cmVhZGVyOnJlYWRlcg==" -v

# POST for reader, results in 403 FORBIDDEN
curl -X POST http://localhost:8080/hello -H "accept: text/plain" -H "Authorization: Basic cmVhZGVyOnJlYWRlcg==" -d "" -v

# GET for writer, results in 403 FORBIDDEN
curl -X GET http://localhost:8080/hello -H "accept: text/plain" -H "Authorization: Basic d3JpdGVyOndyaXRlcg==" -v

# POST for writer, results in 200 OK
curl -X POST http://localhost:8080/hello -H "accept: text/plain" -H "Authorization: Basic d3JpdGVyOndyaXRlcg==" -d "" -v

# GET for admin, results in 200 OK
curl -X GET http://localhost:8080/hello -H "accept: text/plain" -H "Authorization: Basic YWRtaW46YWRtaW4=" -v

# POST for admin, results in 200 OK
curl -X POST http://localhost:8080/hello -H "accept: text/plain" -H "Authorization: Basic YWRtaW46YWRtaW4=" -d "" -v
```

### 2nd test: `rest-assured`

We also have some automated integration tests in [`HelloResourceIT`][helloResourceIT-java]

```java
public class HelloResourceIT {
  
  @Test
  void anonymousCanNotRead() {
    // @formatter:off
    given()
        .when()
            .get()
        .then()
            .statusCode(Status.UNAUTHORIZED.getStatusCode());
    // @formatter:on
  }

  @Test
  void anonymousCanNotWrite() {
    // @formatter:off
    given()
        .when()
            .post()
        .then()
            .statusCode(Status.UNAUTHORIZED.getStatusCode());
    // @formatter:on
  }

  @Test
  void readerCanRead() {
    // @formatter:off
    given()
        .auth()
            .basic("reader", "reader")
        .when()
            .get()
        .then()
            .statusCode(Status.OK.getStatusCode());
    // @formatter:on
  }

  @Test
  void readerCanNotWrite() {
    // @formatter:off
    given()
        .auth()
            .basic("reader", "reader")
        .when()
            .post()
        .then()
        .statusCode(Status.FORBIDDEN.getStatusCode());
    // @formatter:on
  }
  ...
}
```

If we execute those tests
```bash
./mvnww clean verify
```
then all but two tests will fail, reporting a status code of `401` (`UNAUTHORIZED`) instead fo the
expected value.

To ensure that this is not some mistake in the configuration, we can start the application in one
terminal:
```bash
./mvnw quarkus:dev
```
and the tests in another terminal:
```bash
./mvnw -Dquarkus.http.test-host=localhost -Dquarkus.http.test-port=8080 verify
```
The result is the same as before: 6 failed tests, all reporting a status `401`. Notice, however, that
the swagger-ui is still working as expected.

### 3rd test: higher priority
Quarkus has a default `ExceptionMapper` for `UnauthorizedException`s:
`io.quarkus.resteasy.runtime.UnauthorizedExceptionMapper`. This one has a
[`@Priority(5001)`][priortyApi].

If we give our [`UnauthorizedExceptionMapper`][unauthorizedExceptionMapper-java] a higher priority,
it will not be triggered:
```diff
...
@Provider
// one more that io.quarkus.resteasy.runtime.UnauthorizedExceptionMapper's priority:
- // @Priority(5002)
+ @Priority(5002)
public class UnauthorizedExceptionMapper implements ExceptionMapper<UnauthorizedException> {
  ...
}
```
With quarkus in dev mode still running, we can re-execute the integration tests
```bash
./mvnw -Dquarkus.http.test-host=localhost -Dquarkus.http.test-port=8080 verify
```
and see that they now pass.

## Further investigation
If we connect a debugger to the quarkus instance running in dev mode, set a breakpoint in
`io.quarkus.resteasy.runtime.UnauthorizedExceptionMapper` line `46` and trigger a test that expects
a non-`401` status code:
```bash
./mvnw -Dquarkus.http.test-host=localhost -Dquarkus.http.test-port=8080 -Dit.test=HelloResourceIT#adminCanRead verify
```

we see that `challengeData.status` has in fact a value of `401`.

When we make re quest through [`http://localhost:8080/q/swagger-ui`][swagger-localhost] or one of
the curl commands, `io.quarkus.resteasy.runtime.UnauthorizedExceptionMapper` is not triggered.
Especially interesting is the fact that `io.quarkus.resteasy.runtime.UnauthorizedExceptionMapper` is
also triggered, when a `403 FORBIDDEN` is returned, e.g. by this test:
```bash
./mvnw -Dquarkus.http.test-host=localhost -Dquarkus.http.test-port=8080 -Dit.test=HelloResourceIT#readerCanNotWrite verify
```

## Expected behaviour
If the user authenticates with valid credentials, an `ExceptionMapper<UnauthenticatedException>`
should not be triggered

## Observed behaviour
An `ExceptionMapper<UnauthenticatedException>` is triggered.

## Resolution
The implemented exception mapper does not respond with a [http-challenge][httpChallenge]. Therefore
and since restAssured sends credentials lazily, i.e. only when a http-challenge is received, 
authentication fails. We can fix this by letting our 
`UnauthenticatedExceptionMapper extends io.quarkus.resteasy.runtime.UnauthorizedExceptionMapper`,
call it's `toResponse(...)`-method and then set the response body as we see fit:

```
@Provider
public class UnauthorizedExceptionMapper
    extends io.quarkus.resteasy.runtime.UnauthorizedExceptionMapper
    implements ExceptionMapper<UnauthorizedException> {

  @Override
  public Response toResponse(UnauthorizedException exception) {
    return Response.fromResponse(super.toResponse(exception))
        .entity("unauthorized")
        .build();
  }
}
```

[exceptionMapperApi]: https://jakarta.ee/specifications/platform/9/apidocs/jakarta/ws/rs/ext/exceptionmapper
[pom-xml]: ./pom.xml
[application-yml]: ./src/main/resources/application.yml
[forbiddenExceptionMapper-java]: ./src/main/java/de/turing85/exceptionmapper/ForbiddenExceptionMapper.java
[unauthorizedExceptionMapper-java]: ./src/main/java/de/turing85/exceptionmapper/UnauthorizedExceptionMapper.java
[swagger-localhost]: http://localhost:8080/q/swagger-ui
[helloResourceIT-java]: ./src/test/java/de/turing85/HelloResourceIT.java
[priortyApi]: https://jakarta.ee/specifications/platform/8/apidocs/javax/annotation/priority
[httpChallenge]: https://datatracker.ietf.org/doc/html/rfc2617#section-1.2